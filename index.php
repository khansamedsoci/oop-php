<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Array</title>
</head>

<body>
    <?php
    require ("Frog.php");
    require ("Ape.php");

    $sheep = new Animal("shaun");

    echo "Nama Hewan : $sheep->name <br>"; // "shaun"
    echo "Jumlah Kaki : $sheep->legs <br>"; // 2
    echo "Berdarah Dingin : $sheep->cold_blooded<br>"; // false
    echo "<br>";

    $sungokong = new Ape("kera sakti");
    
    echo "Nama Hewan : $sungokong->name <br>";
    echo "Jumlah Kaki : $sungokong->legs <br>";
    echo "Berdarah Dingin : $sungokong->cold_blooded<br>";
    echo $sungokong->yell(); // "Auooo"
    echo "<br><br>";


    $kodok = new Frog("buduk");
    echo "Nama Hewan : $kodok->name <br>";
    echo "Jumlah Kaki : $kodok->legs <br>";
    echo "Berdarah Dingin : $kodok->cold_blooded<br>";
    $kodok->jump(); // "hop hop";

    ?>
</body>

</html>