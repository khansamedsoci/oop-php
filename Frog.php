<?php

require_once("Animal.php");

class Frog extends Animal 
{
    public $legs = 4;
    public $cold_blooded = "true";
    public function jump()
    {
        echo "Suara : ";
        echo "hop hop";
    }
}
